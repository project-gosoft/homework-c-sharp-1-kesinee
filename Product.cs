﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Market
{
    class Product
    {
        private int price;
        private String name;
        public static ArrayList cart = new ArrayList();

        public int getPrice()
        {
            return price;
        }

        public void setPrice(int price)
        {
            this.price = price;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getRecommendation()
        {
            const int highPrice = 100;
            const int mediumPrice = 50;
            if(price > highPrice)
            {
                return "แพง";
            }
            else if (price > mediumPrice)
            {
                return "ธรรมดา";
            }
            else
            {
                return "ถูก";
            }
        }

        public String getCategory()
        {
            if (name.StartsWith("ผัก"))
            {
                return "ผัก";
            }else
            {
                return "ไม่ผัก";
            }
        }

        public double getReucedPrice(String level)
        {
            switch (level)
            {
                case "high":
                    return price * 0.5;
                case "medium":
                    return price * 0.3;
                case "low":
                    return price * 0.1;
            }
            return price;
        }

        public void addToCart()
        {
            Product.cart.Add(this);
        }

        public void printAllProductInCart()
        {
            foreach(Product p in Product.cart)
            {
                Console.WriteLine("Name : " + p.getName());
                Console.WriteLine("Price : " + p.getPrice());
            }
           
        }

        /*static void Main(String [] args)
        {
            Product product = new Product();
            product.setName("ผักกาดขาว");
            //product.setName("ผักคะน้า");
            //product.setPrice(100);
            product.setPrice(150);
        }*/
    }

   
}
